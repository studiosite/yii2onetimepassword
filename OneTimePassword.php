<?php

namespace studiosite\yii2onetimepassword;

use Yii;
use yii\bootstrap\InputWidget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Button;

/**
 * Виджет отрисовки инпута для ввода одноразового пароля
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 */
class OneTimePassword extends InputWidget
{
    /**
     * @var string|array the route of the action that generates the CAPTCHA images.
     * The action represented by this route must be an action of [[OneTimePasswordAction]].
     * Please refer to [[\yii\helpers\Url::toRoute()]] for acceptable formats.
     */
    public $generatorAction = 'site/one-time-password';

    /**
    * @var string Шаблон
    */
    public $template = '{input} {refreshLink}';

    /**
    * @var array Опции по умолчанию
    */
    public $options = [
        'class' => 'form-control',
    ];

    /**
    * @var array Параметры виджета кнопки Button::widget
    */
    public $refreshButton = [
        'class' => 'form-control',
    ];

    /**
    * @var array Параметры, которые будут отправлены генератору
    */
    public $generatorParams = [];

    /**
    * При инициализации настройка языка
    */
    public function init()
    {
        Yii::setAlias('@studiosite/yii2onetimepassword', __DIR__);

        if (!isset(Yii::$app->i18n->translations['yii2onetimepassword'])) {
            Yii::$app->i18n->translations['yii2onetimepassword'] = [
                'class' => 'yii\i18n\PhpMessageSource',
                'sourceLanguage' => 'en-US',
                'basePath' => '@studiosite/yii2onetimepassword/messages',
                'forceTranslation' => true,
                'fileMap' => [
                    'yii2onetimepassword' => 'messages.php'
                ]
            ];
        }

        parent::init();
    }

    /**
     * Renders the widget.
     */
    public function run()
    {
        if (!isset($this->options['id']))
            $this->options['id'] = $this->getId();

        if ($this->hasModel()) {
            $input = Html::activeTextInput($this->model, $this->attribute, $this->options);
        } else {
            $input = Html::textInput($this->name, $this->value, $this->options);
        }

        if (!isset($this->refreshButton['id']))
            $this->refreshButton['id'] = $this->options['id']."_refreshButton";

        $linkOptions = ArrayHelper::merge([
            'label' => Yii::t('yii2onetimepassword', 'Refresh code'),
            'options' => ['class' => 'btn-link', 'type' => 'button'],
        ], $this->refreshButton);
        $linkRefresh = Button::widget($linkOptions);

        $this->registerClientScript($this->options['id'], $this->refreshButton['id']);

        return strtr($this->template, [
            '{input}' => $input,
            '{refreshLink}' => $linkRefresh,
        ]);
    }

    /**
     * Registers the needed JavaScript.
     * 
     * @param string id refresh buttons
     */
    public function registerClientScript($id, $buttonId)
    {
        $view = $this->getView();

        $route = $this->generatorAction;
        if (is_array($route)) {
            $route['v'] = uniqid();
        } else {
            $route = [$route, 'v' => uniqid()];
        }

        $checkRoute = $refreshRoute = $route;
        $checkRoute['action'] = 'check';
        $refreshRoute['action'] = 'refresh';

        $view->registerJs("
            {
                var addErrorCodeInput = function(helpBlock, message) {
                    helpBlock.text(message);

                    helpBlock.addClass('help-block-error');
                    helpBlock.removeClass('help-block-success');
                    
                    helpBlock.parent().removeClass('has-success');
                    helpBlock.parent().addClass('has-error');

                    helpBlock.show();
                };

                var addMessageCodeInput = function(helpBlock, message) {

                    helpBlock.text(message);
                    helpBlock.addClass('help-block-success');
                    helpBlock.removeClass('help-block-error');

                    helpBlock.parent().removeClass('has-error');
                    helpBlock.parent().addClass('has-success');

                    helpBlock.show();
                };

                jQuery.post('".Url::to($checkRoute)."', {
                    params: ".Json::encode($this->generatorParams)."
                }, function(data) {
                    if (data && data.error) {
                        addErrorCodeInput(jQuery('#$id').parent().find('.help-block'), data.error);
                    } else if (data && data.message) {
                        addMessageCodeInput(jQuery('#$id').parent().find('.help-block'), data.message);
                    }
                }, 'json');


                jQuery('#$buttonId').click(function(e) {
                    var button = jQuery(this);
                    e.preventDefault();
                    button.hide();

                    jQuery.post('".Url::to($refreshRoute)."', {
                        params: ".Json::encode($this->generatorParams)."
                    }, function(data) {
                        button.show();
                        if (data && data.error) {
                            addErrorCodeInput(jQuery('#$id').parent().find('.help-block'), data.error);
                        } else if (data && data.message) {
                            addMessageCodeInput(jQuery('#$id').parent().find('.help-block'), data.message);
                        }
                    }, 'json');

                });
            }
        ");
    }
}