<?php

namespace studiosite\yii2onetimepassword;

use Yii;
use yii\web\Response;
use yii\base\InvalidRouteException;
use yii\base\InvalidConfigException;

/**
 * Экшен генератора одноразовых паролей
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 */
class OneTimePasswordAction extends \yii\base\Action
{
	/**
    * @var string|Closure|null Генератор одноразывах паролей
    */
    public $codeGenerate;

    /**
    * @var string|Closure|null Лимит генерации кодов 
    */
    public $codeGenerateLimit = 3;

    /**
    * @var integer Таймаут между получением новых codeGenerateLimit попыток в секундах
    */
    public $codeGenerateTimeout = 1000;

    /**
    * @var Closure Вызывается в случае исчерпания лимита отправки паролей
    */
    public $limitClosure;

    /**
    * @var integer Лимит валидаций
    */
    public $attemptValidate = 3;

	/**
	* @var string Префикс ключа сессии
	*/
	public $sessionKeyPrefix = '';

	/**
	* @var integer Таймаут между повторной серией генерации в секундах
	*/
	public $sessionTimeOut = 1000;

    /**
    * @var string Сообщение при неудачной попытке генерации кода
    */
    public $messageGenerateFailed;

    /**
    * @var string Сообщение при удачной попытке генерации кода
    */
    public $messageGenerateSuccess;

    /**
    * При инициализации настройка языка
    */
    public function init()
    {
        Yii::setAlias('@studiosite/yii2onetimepassword', __DIR__);

        if (!isset(Yii::$app->i18n->translations['yii2onetimepassword'])) {
            Yii::$app->i18n->translations['yii2onetimepassword'] = [
                'class' => 'yii\i18n\PhpMessageSource',
                'sourceLanguage' => 'en-US',
                'basePath' => '@studiosite/yii2onetimepassword/messages',
                'forceTranslation' => true,
                'fileMap' => [
                    'yii2onetimepassword' => 'messages.php'
                ]
            ];
        }

        parent::init();
    }

	/**
	* Запуск
	*
	* @param string $action
	* @return array
	*/
	public function run($action, $params = null)
	{
		Yii::$app->response->format = Response::FORMAT_JSON;
		$param = Yii::$app->request->post('params', []);
		$currentCode = $this->getCurrentCode();
        $attemptValidate = $this->getAttemptValidate();

        if ($attemptValidate>$this->attemptValidate || $action=='refresh') {
            $this->getSession()->remove($this->getSessionKey());
            $this->getSession()->remove($this->getSessionKey()."/codeAttemptCount");
            $attemptValidate = 0;
            $currentCode = false;
        }

        if (!$currentCode) { // Кода нет
          	
          	$generateCount = $this->getGenerateCount();
          	$lastGenerateTime = $this->getLastGenerateTime();

            if ($generateCount<$this->codeGenerateLimit || ($lastGenerateTime+$this->sessionTimeOut)>time()) { // Не исчерпан лимит генераций кода

                // Сброс количества генераций
                if ($generateCount<$this->codeGenerateLimit) {
                    $this->getSession()->set($this->getSessionKey()."/codeGeneratedCount", 0);
                }

                $code = $this->generateCode($param);

                if (empty($code)) {
                	return [
	                	'error' => $this->messageGenerateFailed ?: Yii::t('yii2onetimepassword', 'Cant generate code')
	                ];
                }

                $this->getSession()->set($this->getSessionKey(), $code);
                $this->getSession()->set($this->getSessionKey()."/lastCodeTime", time());
                $this->getSession()->set($this->getSessionKey()."/codeGeneratedCount", $generateCount+1);

                return [
                	'message' => $this->messageGenerateSuccess ?: Yii::t('yii2onetimepassword', 'The new code successfully generated')
                ];

            } else {
                return [
                	'error' => $this->getLimitClosure()
                ];
            }
        }

		return [];
	}	

	/**
	* Валидация кода
	*
	* @param mixen $value Значение из формы
	* @return boolean
	*/
	public function validate($value)
	{
		$curentCode = $this->getCurrentCode();

		if (!$curentCode) {
			$this->getSession()->set($this->getSessionKey()."/codeAttemptCount", $this->getAttemptValidate()+1);

		    return OneTimePasswordValidator::FORM_IS_OUTDATED;
		}

		if ($curentCode!=$value) {
			$this->getSession()->set($this->getSessionKey()."/codeAttemptCount", $this->getAttemptValidate()+1);

		    return OneTimePasswordValidator::INCORECT_VALUE;
		}

		return;
	}

	/**
     * Returns the session variable name used to store verification code.
     *
     * @return string the session variable name
     */
    protected function getSessionKey()
    {
        return '_'.$this->sessionKeyPrefix.'_onTimePassword/' . $this->getUniqueId();
    }

    /**
    * Генератор кодов
    *
    * @return mixen Код
    */
    protected function generateCode($params)
    {
        $codeGenerate = $this->codeGenerate;
        $codeGenerateCall= is_callable($this->codeGenerate) ? $this->codeGenerate : function($params) {
            return 'test-code';
        };
        return $codeGenerateCall($params);
    }

    private $_session;

    /**
    * Сессия
    *
    * @return Session
    */
    protected function getSession()
    {
        if (empty($this->_session)) {
            $this->_session = Yii::$app->getSession();
            $this->_session->open();
        }

        return $this->_session;
    }

    /**
    * Получить текущий код из сессии
    *
    * @return mixen
    */
    protected function getCurrentCode()
    {
        return $this->getSession()->get($this->getSessionKey(), false);
    }

    /**
    * Время последней генерации кода
    *
    * @return interger
    */
    protected function getLastGenerateTime()
    {
        return $this->getSession()->get($this->getSessionKey()."/lastCodeTime", 0);
    }

    /**
    * Количество генераций кода
    *
    * @return integer
    */
    protected function getGenerateCount()
    {
        return $this->getSession()->get($this->getSessionKey()."/codeGeneratedCount", 0);
    }

    /**
    * Количество попыток валидации
    *
    * @return interger
    */
    protected function getAttemptValidate()
    {
        return $this->getSession()->get($this->getSessionKey()."/codeAttemptCount", 0);
    }

    /**
    * Сообщение в случае исчерпания лимита отправок
    *
    * @return string
    */
    protected function getLimitClosure()
    {
        $limitClosure = $this->limitClosure;
        $limitClosureCall= is_callable($this->limitClosure) ? $this->limitClosure : function($widget) {
            return Yii::t('yii2onetimepassword', 'Limit sending one-time passwords');
        };
        
        return $limitClosureCall($this);
    }

    /**
    * Очистка кода и попыток валидации
    */
    public function clearCode()
    {
    	$this->getSession()->remove($this->getSessionKey());
    	$this->getSession()->remove($this->getSessionKey()."/codeAttemptCount");
    }


   /**
    * Полная очистка всех хранимых ключей
    */
    public function clearAllSessionKeys()
    {
    	$this->getSession()->remove($this->getSessionKey());
    	$this->getSession()->remove($this->getSessionKey()."/codeAttemptCount");
    	$this->getSession()->remove($this->getSessionKey()."/lastCodeTime");
    	$this->getSession()->remove($this->getSessionKey()."/codeGeneratedCount");
    }

    /**
    * Очистка сессии для экшена
    *
    * @throws InvalidConfigException
    * @param string $route
    */
    public static function getAction($route)
    {
    	$pa = Yii::$app->createController($route);
        if ($pa !== false) {
            /* @var $controller \yii\base\Controller */
            list($controller, $actionID) = $pa;
            $action = $controller->createAction($actionID);
            if ($action !== null) {
                return $action;
            }
        }
        throw new InvalidConfigException(Yii::t('yii2onetimepassword', 'Invalid one-time-password action ID {id}', ['id' => $route]));
    }
}