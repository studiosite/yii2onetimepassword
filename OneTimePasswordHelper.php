<?php

namespace studiosite\yii2onetimepassword;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * Хелпер для генерации одноразовых паролей
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 */
 class OneTimePasswordHelper
 {
    /**
    * Генерация случайногно кода с ограниченным числом уникальных цыфр
    *
 	* @param int $length длинна число
 	* @param int $uNumber колличество уникальных цыфр
 	* @return string случайное число строкой
 	*/
 	public static function numberCode($length = 5, $uNumber = 3)
 	{
 		$uNumbers = array();
 		$code = "";

 		for($i=0; $i<$uNumber; $i++) {
 			$uNumbers[]=rand(0,9);
 		}

 		for($i=0; $i<$length; $i++) {
 			$code.=$uNumbers[rand(0,$uNumber-1)];
 		}

 		return $code;
 	}
 }
