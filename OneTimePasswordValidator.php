<?php

namespace studiosite\yii2onetimepassword;

use Yii;
use yii\base\InvalidConfigException;

/**
 * Валидатор одноразовых паролей
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 */
class OneTimePasswordValidator extends \yii\validators\Validator
{
	/**
	* @const integer Ошибка валидации - форма устарела
	*/
	const FORM_IS_OUTDATED = 1;

	/**
	* @const integer Ошибка валидации - неверный код
	*/
	const INCORECT_VALUE = 2;

	/**
    * @var boolean whether this validation rule should be skipped if the attribute value
    * is null or an empty string.
    */
    public $skipOnEmpty = false;

    /**
    * @var string|array the route of the action that generates the CAPTCHA images.
    * The action represented by this route must be an action of [[OneTimePasswordAction]].
    * Please refer to [[\yii\helpers\Url::toRoute()]] for acceptable formats.
    */
    public $generatorAction = 'site/one-time-password';

    /**
    * @var string Сообщение в случае если форма устарела
    */
    public $messageFormOutdated;

	/**
	* Валидация кода
	*
	* @param ModelInterface $model Модель
	* @param string $attribute Имя атрибута
	*/
	public function validateAttribute($model, $attribute)
	{
		$action = OneTimePasswordAction::getAction($this->generatorAction);
        $validResponse = $action->validate($model->$attribute);

        $this->message = $this->message ?: Yii::t('yii2onetimepassword', 'One-time password is entered incorrectly');
        $this->messageFormOutdated ?: Yii::t('yii2onetimepassword', 'Form is outdated. Refresh the page');
        
        if ($validResponse!==null) {
        	switch ($validResponse) {
        		case self::FORM_IS_OUTDATED:
        			$this->addError($model, $attribute, $this->messageFormOutdated);
        			return [$this->messageFormOutdated, []];
        		
        		default:
        			$this->addError($model, $attribute, $this->message);
        			return [$this->message, []];
        	}


        }

        return null;
	}

    /**
    * При инициализации настройка языка
    */
    public function init()
    {
        Yii::setAlias('@studiosite/yii2onetimepassword', __DIR__);

        if (!isset(Yii::$app->i18n->translations['yii2onetimepassword'])) {
            Yii::$app->i18n->translations['yii2onetimepassword'] = [
                'class' => 'yii\i18n\PhpMessageSource',
                'sourceLanguage' => 'en-US',
                'basePath' => '@studiosite/yii2onetimepassword/messages',
                'forceTranslation' => true,
                'fileMap' => [
                    'yii2onetimepassword' => 'messages.php'
                ]
            ];
        }

        parent::init();
    }
}