yii2onetimepassword
=================

Одноразовые пароли, экшен, валидатор, хелпер

## Установка

Предпочтительный способ установить это расширение через композитор. [composer](http://getcomposer.org/download/).

```
"studiosite/yii2onetimepassword": "*"
```

в секции ```require``` `composer.json` файла.

## Использование

Для каждой формы (отдельного одноразового пароля), в контроллере, должен быть прописан экшен.

Пример:

```php
...
class TestController extends Controller
{
...
public function actions()
    {
        return [
            'one-time-password' => [
                'class' => \studiosite\yii2onetimepassword\OneTimePasswordAction::className(),
                'codeGenerate' => function($params) {
                
                    $code = \studiosite\yii2onetimepassword\OneTimePasswordHelper::numberCode();
                    $phone = substr(preg_replace("/[^0-9]/", "",  $params['phone']), 1);
                    $messageId = Yii::$app->sms->send($phone, "Код: ".$code);

                    if (!$messageId)
                        throw new \Exception(print_r(Yii::$app->sms->errors, true));

                    return $code;
                },
            ],
            ...
        ];
    }
...
}

```

В этом примере в качестве генератора одноразового пароля выступает хелпер из пакета ```OneTimePasswordHelper``` и отправка этого кода по смс.

Далее, в отображении формы, необходимо отрисовать виджет, который будет ссылаться на ранее прописаный экшен

```php
...
echo $form->field($model, 'oneTimePassword')->widget(\studiosite\yii2onetimepassword\OneTimePassword::className(), [
                    'generatorAction' => 'test/one-time-password',
                    'generatorParams' => [
                        'phone' => $model->phone,
                    ]
                ]) ?>
...
```

Где ```generatorParams``` массив параметрок, которые будут переданы генератору, ```generatorAction``` - строка роута

Далее в форме, необходимо прописать валидатор:

```php

public function rules()
{
     return [
         [
            ['oneTimePassword'], 
            'studiosite\yii2onetimepassword\OneTimePasswordValidator', 
            'on' => 'loginConfirm',
            'generatorAction' => '/custom/test/one-time-password'
         ],
     ];
}
```

обязательно указав при этом ```generatorAction``` роут на ранее прописаный экшен

После успешной валидации, в контроллере, необходимо выполнить очистку текущего кода, для автоматической генерации в следующий раз

```php

if ($model->validate()) {
    \studiosite\yii2onetimepassword\OneTimePasswordAction::getAction('custom/test/one-time-password')->clearCode();

    $model->save();
}
```